const express = require('express');
const router = new express.Router();
const Axios = require('axios');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/pantry', async (req, res, next) => {
  try {
    const response = await Axios.get(process.env.INVENTORY_URL + '/api/pantry', {
      headers: {
        Authorization: req.session.token,
      },
    });
    res.status(200).json(response.data);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
