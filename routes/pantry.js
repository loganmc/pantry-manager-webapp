const express = require('express');
const router = express.Router();
const Axios = require('axios');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.get('/add', async (req, res, next) => {
  try {
    res.render('new-food');
  } catch (error) {
    next(error);
  }
});

router.post('/add', async (req, res, next) => {
  try {
    const name = req.body.name;
    const expirationDate = req.body.expirationDate;
    const token = req.session.token;

    const response = await Axios.post(process.env.INVENTORY_URL + '/api/pantry/food', {
      name: name,
      expirationDate: expirationDate,
    }, {
      headers: {
        Authorization: token,
      },
    });

    console.log(response);
    res.redirect('/');
  } catch (error) {
    next(error);
  }
});

router.get('/food/:foodId/delete', async (req, res, next) => {
  try {
    const foodId = req.params.foodId;
    const token = req.session.token;

    const response = await Axios.delete(process.env.INVENTORY_URL + '/api/pantry/food', {
      data: {
        foodId: foodId,
      },
      headers: {
        Authorization: token,
      },
    });

    res.redirect('/');
  } catch (error) {
    next(error);
  }
});

module.exports = router;
