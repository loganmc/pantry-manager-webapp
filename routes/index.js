const express = require('express');
const router = new express.Router();
const Axios = require('axios');
/* GET home page. */
router.get('/', async (req, res, next) => {
  try {
    let user = null;
    let foods = [];

    if (req.session.token != null) {
      response = await Axios.get(process.env.ACCOUNT_URL + '/api/user', {
        headers: {
          Authorization: req.session.token,
        },
      });
      user = response.data.user;
      foods = await Axios.get(process.env.INVENTORY_URL + '/api/pantry', {
        headers: {
          Authorization: req.session.token,
        },
      });
      foods = foods.data.pantry.foods;
      foods = foods.sort((a, b) => {
        return new Date(a.expirationDate) > new Date(b.expirationDate);
      });

      let ingredients = '';
      for (const food of foods) {
        ingredients += (encodeURIComponent(food.name) + ',');
      }

      recipiesResult = await Axios.get(process.env.RECIPES_URL + `/api/recipie?ingredients=${ingredients}`);
      recipies = recipiesResult.data.data;
    }
    res.render('index', {
      'title': process.env.SERVICE_NAME,
      'user': user,
      'pantry': foods,
      'recipies': recipies,
    });
  } catch (error) {
    next(error);
  }
});

router.get('/signin', (req, res, next) => {
  res.render('signin');
});

router.post('/signin', async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;

    const result = await Axios.post(process.env.ACCOUNT_URL + '/api/signin', {
      email: email,
      password: password,
    });


    req.session.token = result.data.token;
    res.redirect('/');
  } catch (error) {
    next(error);
  }
});

router.get('/signup', (req, res, next) => {
  res.render('signup');
});

router.post('/signup', async (req, res, next) => {
  try {
    const response = await Axios.post(process.env.ACCOUNT_URL + '/api/signup', {
      email: req.body.email,
      password: req.body.password,
    });
    console.log(response);
    req.session.token = response.data;
    res.redirect('/signin');
  } catch (error) {
    next(error);
  }
});

router.get('/recipe/:recipeId', async (req, res, next) => {
  try {
    const recipeId = req.params.recipeId;
    const response = await Axios.get(process.env.RECIPES_URL + '/api/recipe/' + recipeId);

    res.render('recipe', {
      recipe: response.data.recipe,
    });
    console.log(response);
  } catch (error) {
    console.log(error);
    next(error);
  }
});


module.exports = router;
